import com.bmuschko.gradle.docker.tasks.image.*
import com.github.jengelman.gradle.plugins.shadow.tasks.*

application.mainClassName = "io.ktor.server.netty.EngineMain"

plugins {
    id("application")
    id("com.bmuschko.docker-remote-api") version "6.1.2"
    id("com.github.johnrengelman.shadow") version "4.0.3"
    kotlin("jvm") version "1.3.61"
}

group = "com.asseco.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    val kotlinVersion = "1.3.61"
    val groovyVersion = "2.4.15"
    val ktorVersion = "1.2.2"
    val ktorSwaggerVersion = "0.5.0"
    val koinVersion = "2.0.1"
    val c3p0Version = "0.9.5.4"
    val sql2oVersion = "1.6.0"
    val h2Version = "1.4.200"
    val logbackVersion = "1.2.3"
    val snakeyamlVersion = "1.25"
    val postgresVersion = "42.2.9"

    val kotlinTestVersion = "3.3.3"
    val mockkVersion = "1.9.3.kotlin12"

    implementation(kotlin("stdlib-jdk8", kotlinVersion))
    implementation("org.codehaus.groovy:groovy:$groovyVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-gson:$ktorVersion")
    implementation("de.nielsfalk.ktor:ktor-swagger:$ktorSwaggerVersion")
    implementation("org.koin:koin-core:$koinVersion")
    implementation("com.mchange:c3p0:$c3p0Version")
    implementation("org.sql2o:sql2o:$sql2oVersion")
    implementation("com.h2database:h2:$h2Version")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.yaml:snakeyaml:$snakeyamlVersion")
    implementation("org.postgresql:postgresql:$postgresVersion")

    testImplementation("org.koin:koin-test:$koinVersion")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:$kotlinTestVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}

@Suppress("DEPRECATION")
tasks.withType<ShadowJar> {
    baseName="app"
    classifier = ""
    version = ""
}

tasks.create("copyFiles", type = Copy::class) {
    dependsOn("shadowJar")

    from(file("$buildDir")) {
        include("libs/*", "resources/main/**")
    }
    into(file("$buildDir/docker"))
}


tasks.create("buildDockerImage", DockerBuildImage::class) {
    dependsOn("copyFiles")

    inputDir.set(project.projectDir)
    images.add("example/library:latest")
}