FROM ringcentral/jdk:latest
MAINTAINER Mateusz Boral <mateusz.boral@asseco.pl>

COPY ./build/docker/libs /usr/src
WORKDIR /usr/src

ENTRYPOINT ["java", "-jar", "app.jar"]