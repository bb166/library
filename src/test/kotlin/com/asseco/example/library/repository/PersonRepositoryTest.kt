package com.asseco.example.library.repository

import com.asseco.example.library.domain.entity.Person
import com.asseco.example.library.util.Configurator
import io.kotlintest.Spec
import io.kotlintest.shouldNotThrowAny
import io.kotlintest.specs.StringSpec
import io.mockk.every
import io.mockk.mockk
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.sql2o.Sql2o

class PersonRepositoryTest: KoinTest, StringSpec({
    "get person by pesel when pesel number is available in database" {
        shouldNotThrowAny { PersonRepository.getPersonByPesel(correctPersonEntity1.pesel) }
    }

    "delete person when his pesel is available in database" {
        shouldNotThrowAny { PersonRepository.deletePerson(correctPersonEntity1.pesel) }
    }

    "add person when the person is not available in database" {
        shouldNotThrowAny { PersonRepository.addPerson(correctPersonEntity2) }
    }
}) {
    override fun beforeSpec(spec: Spec) {
        startKoin {
            modules(listOf(module {
                single {
                    mockk<Configurator> {
                        every { string("db.url") } returns "postgres"
                    }
                }
                single {
                    mockk<Sql2o> {
                        every { open() } returns mockk {
                            val connection = this

                            every { createQuery(PostgreSqlQueries.SELECT_PERSON_BY_PESEL) } returns mockk {
                                every { addParameter("pesel", correctPersonEntity1.pesel) } returns this
                                every { executeAndFetch(Person::class.java) } returns listOf(correctPersonEntity1)
                            }

                            every { createQuery(PostgreSqlQueries.CHECK_PERSON_AVAILABILITY_IN_DATABASE) } returns mockk {
                                every { addParameter("pesel", correctPersonEntity1.pesel) } returns mockk { every { executeScalar() } returns 1 }
                                every { addParameter("pesel", correctPersonEntity2.pesel) } returns mockk { every { executeScalar() } returns null }
                            }

                            every { createQuery(PostgreSqlQueries.DELETE_PERSON) } returns mockk {
                                every { addParameter("pesel", correctPersonEntity1.pesel) } returns this
                                every { executeScalar() } returns 1
                            }

                            every { createQuery(PostgreSqlQueries.CHECK_PERSON_DELETE_STATE) } returns mockk {
                                every { addParameter("pesel", correctPersonEntity1.pesel) } returns this
                                every { executeScalar() } returns null
                            }

                            every { createQuery(PostgreSqlQueries.DELETE_PERSON) } returns mockk {
                                every { addParameter("pesel", correctPersonEntity1.pesel) } returns this
                                every { executeUpdate() } returns connection
                            }

                            every { createQuery(PostgreSqlQueries.CHECK_PERSON_AVAILABILITY) } returns mockk {
                                every { addParameter("pesel") }
                                every { executeScalar() } returns 1
                            }

                            every { createQuery(PostgreSqlQueries.INSERT_PERSON) } returns mockk {
                                every { addParameter("firstName", correctPersonEntity2.firstName) } returns this
                                every { addParameter("lastName", correctPersonEntity2.lastName) } returns this
                                every { addParameter("phoneNumber", correctPersonEntity2.phoneNumber) } returns this
                                every { addParameter("pesel", correctPersonEntity2.pesel) } returns this
                                every { executeUpdate() } returns connection
                            }

                            every { close() } returns Unit
                        }
                    }
                }
            }))
        }
    }

    override fun afterSpec(spec: Spec) {
        stopKoin()
    }

    companion object {
        val correctPersonEntity1 = Person(firstName = "Jan", lastName = "Kowalski", pesel = "87111254321",  phoneNumber = "711355123")
        val correctPersonEntity2 = Person(firstName = "Jan", lastName = "Kowalski", pesel = "85111284321",  phoneNumber = "711355123")
    }
}