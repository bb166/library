package com.asseco.example.library.repository

import com.asseco.example.library.domain.entity.Book
import com.asseco.example.library.exception.BookError
import com.asseco.example.library.exception.UserErrorException
import com.asseco.example.library.util.Configurator
import io.kotlintest.Spec
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotThrowAny
import io.kotlintest.shouldThrowAny
import io.kotlintest.specs.StringSpec
import io.mockk.every
import io.mockk.mockk
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.sql2o.Sql2o

class BookRepositoryTest: KoinTest, StringSpec({
    "add book when the book with given isbn is available in database" {
        shouldThrowAny { BookRepository.addBook(wrongBook) } shouldBe UserErrorException(BookError.ALREADY_AVAILABLE.toStatusBody())
    }

    "add book when the book with given isbn isn't available in database" {
        shouldNotThrowAny { BookRepository.addBook(correctBook) shouldBe Unit }
    }

    "delete book when the book with given isbn isn't available in database" {
        shouldThrowAny { BookRepository.deleteBook(isbnIsNotAvailableInDatabase) } shouldBe UserErrorException(BookError.NOT_FOUND.toStatusBody())
    }

    "delete book when the book with given isbn is already deleted" {
        shouldThrowAny { BookRepository.deleteBook(isbnIsAlreadyDeleted) } shouldBe UserErrorException(BookError.ALREADY_DELETED.toStatusBody())
    }

    "delete book when isbn is correct" {
        shouldNotThrowAny { BookRepository.deleteBook(isbnCanBeSafelyRemoved) shouldBe Unit }
    }

    "get all rented books for user" {
        shouldNotThrowAny { BookRepository.getRentedBooksForUser(correctPesel) shouldBe listOf(correctBook) }
    }

    "rent book by user" {
        shouldNotThrowAny { BookRepository.rentBookByUser(correctPesel, correctBook.isbn) }
    }
}) {
    override fun beforeSpec(spec: Spec) {
        startKoin {
            modules(listOf(module {
                single {
                    mockk<Configurator> {
                        every { string("db.url") } returns "postgres"
                    }
                }
                single {
                    mockk<Sql2o>{
                        every { open() } returns mockk {
                            val connection = this

                            every { createQuery(PostgreSqlQueries.CHECK_BOOK_AVAILABILITY) } returns mockk {
                                every { addParameter("isbn", wrongBook.isbn) } returns mockk { every { executeScalar() } returns 1 }
                                every { addParameter("isbn", correctBook.isbn) } returns mockk { every { executeScalar() } returns null }
                                every { addParameter("isbn", isbnIsAlreadyDeleted) } returns mockk { every { executeScalar() } returns 1 }
                                every { addParameter("isbn", isbnIsNotAvailableInDatabase) } returns mockk { every { executeScalar() } returns null }
                                every { addParameter("isbn", isbnCanBeSafelyRemoved) } returns mockk { every { executeScalar() } returns 1 }
                            }

                            every { createQuery(PostgreSqlQueries.DELETE_BOOK) } returns mockk {
                                every { addParameter("isbn", isbnCanBeSafelyRemoved) } returns this
                                every { executeUpdate() } returns connection
                            }

                            every { createQuery(PostgreSqlQueries.CHECK_PERSON_AVAILABILITY) } returns mockk {
                                every { addParameter("pesel", correctPesel) } returns this
                                every { executeScalar() } returns 1
                            }

                            every { createQuery(PostgreSqlQueries.SELECT_RENTED_BOOKS_BY_PERSON) } returns mockk {
                                every { addParameter("pesel", correctPesel) } returns this
                                every { executeAndFetch(Book::class.java) } returns listOf(correctBook)
                            }

                            every { createQuery(PostgreSqlQueries.CHECK_BOOK_DELETE_STATE) } returns mockk {
                                every { addParameter("isbn", isbnIsAlreadyDeleted) } returns mockk { every { executeScalar() } returns 1 }
                                every { addParameter("isbn", isbnCanBeSafelyRemoved) } returns mockk { every { executeScalar() } returns null}
                            }

                            every { createQuery(PostgreSqlQueries.INSERT_BOOK) } returns mockk {
                                every { addParameter("isbn", correctBook.isbn) } returns this
                                every { addParameter("title", correctBook.title) } returns this
                                every { addParameter("pageCount", correctBook.pageCount) } returns this
                                every { addParameter("author", correctBook.author) } returns this
                                every { executeUpdate() } returns connection
                            }

                            every { createQuery(PostgreSqlQueries.SELECT_ID_FOR_PERSON) } returns mockk {
                                every { addParameter("pesel", correctPesel) } returns mockk { every { executeScalar() } returns 1 }
                            }

                            every { createQuery(PostgreSqlQueries.SELECT_ID_FOR_BOOK) } returns mockk {
                                every { addParameter("isbn", correctBook.isbn) } returns mockk { every { executeScalar() } returns 1 }
                            }

                            every { createQuery(PostgreSqlQueries.INSERT_BOOK_RENT_TO_PERSON) } returns mockk {
                                every { addParameter("bookId", 1) } returns this
                                every { addParameter("personId", 1) } returns this
                                every { executeUpdate() } returns connection
                            }

                            every { close() } returns Unit
                        }
                    }
                }
            }))
        }
    }

    override fun afterSpec(spec: Spec) {
        stopKoin()
    }

    companion object {
        const val isbnCanBeSafelyRemoved = "321-333-111-41-17"
        const val isbnIsNotAvailableInDatabase = "321-333-111-41-15"
        const val isbnIsAlreadyDeleted = "321-333-111-41-20"
        const val correctPesel = "88111154321"
        val wrongBook = Book(isbn = "321-333-111-41-12", title = "test", pageCount = 100, author = "Jan Kowalski")
        val correctBook = Book(isbn = "321-333-111-41-16", title = "test2", pageCount = 103, author = "Jan Kowalski")
    }
}