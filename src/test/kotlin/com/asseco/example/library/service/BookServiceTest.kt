package com.asseco.example.library.service

import com.asseco.example.library.domain.dto.BookDTO
import com.asseco.example.library.domain.entity.Book
import com.asseco.example.library.exception.*
import com.asseco.example.library.repository.BookRepository
import io.kotlintest.Spec
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotThrowAny
import io.kotlintest.shouldThrowAny
import io.kotlintest.specs.StringSpec
import io.mockk.every
import io.mockk.mockk
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest

class BookServiceTest : KoinTest, StringSpec({
    "add book when isbn number have wrong length" {
        shouldThrowAny { BookService.addBook(wrongBookDto) } shouldBe
                UserErrorsException(listOf(BookError.ISBN_LENGTH.toStatusBody()))
    }

    "delete book when isbn is not available" {
        shouldThrowAny {
            BookService.deleteBook(isbnNotAvailableInDatabase)
        } shouldBe UserErrorException(BookError.NOT_FOUND.toStatusBody())
    }

    "get rented books by user when the user pesel is not available in the database" {
        shouldThrowAny {
            BookService.getRentedBooksForUser(peselNotAvailableInDatabase)
        } shouldBe UserErrorException(PersonError.PERSON_NOT_FOUND.toStatusBody())
    }

    "get rented books by user when the user pesel is available in the database" {
        shouldNotThrowAny {
            BookService.getRentedBooksForUser(peselAvailableInDatabase) shouldBe
                    listOf(correctBook1Dto)
        }
    }

    "rent book by user when user and book are available in database" {
        shouldNotThrowAny {
            BookService.rentBookByUser(peselAvailableInDatabase, isbnAvailableInDatabase)
        }
    }
}) {
    override fun beforeSpec(spec: Spec) {
        startKoin {
            modules(
                listOf(
                    module {
                        single {
                            mockk<BookRepository> {
                                every { deleteBook(isbnNotAvailableInDatabase) } throws UserErrorException(BookError.NOT_FOUND.toStatusBody())
                                every { getRentedBooksForUser(peselNotAvailableInDatabase) } throws UserErrorException(PersonError.PERSON_NOT_FOUND.toStatusBody())
                                every { getRentedBooksForUser(peselAvailableInDatabase) } returns listOf(correctBook1Entity)
                                every { rentBookByUser(peselAvailableInDatabase, isbnAvailableInDatabase) } returns Unit
                            }
                        }
                    }
                )
            )
        }
    }

    override fun afterSpec(spec: Spec) {
        stopKoin()
    }

    companion object {
        const val peselNotAvailableInDatabase = "88112212345"
        const val peselAvailableInDatabase = "88112212346"
        const val isbnNotAvailableInDatabase = "321-333-111-41-12"
        const val isbnAvailableInDatabase = "321-333-111-41-11"
        val wrongBookDto = BookDTO(title = "Test", author = "Jan Kowalski", isbn = "321-333-111-41-1", pageCount = 123)
        val correctBook1Dto = BookDTO(title = "Test", author = "Jan Kowalski", isbn = "321-333-111-41-12", pageCount = 123)
        val correctBook1Entity = Book(title = "Test", author = "Jan Kowalski", isbn = "321-333-111-41-12", pageCount = 123)
    }
}
