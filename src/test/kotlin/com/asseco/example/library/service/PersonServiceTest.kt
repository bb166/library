package com.asseco.example.library.service

import com.asseco.example.library.domain.dto.PersonDTO
import com.asseco.example.library.domain.entity.Person
import com.asseco.example.library.exception.PersonError
import com.asseco.example.library.exception.UserErrorException
import com.asseco.example.library.exception.UserErrorsException
import com.asseco.example.library.repository.PersonRepository
import io.kotlintest.*
import io.kotlintest.specs.StringSpec
import io.mockk.every
import io.mockk.mockk
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest

class PersonServiceTest: KoinTest, StringSpec ({
    "get person when pesel number isn't available in database" {
        shouldThrowAny { PersonService.getUserByPesel(peselNotAvailableInDatabase) } shouldBe
                UserErrorException(PersonError.PERSON_NOT_FOUND.toStatusBody())
    }

    "get person when pesel number available in database" {
        shouldNotThrowAny { PersonService.getUserByPesel(correctPerson1Entity.pesel) shouldBe correctPerson1Dto }
    }

    "change phone number when not valid" {
        shouldThrowAny { PersonService.changePhoneNumber(correctPerson1Dto.pesel, wrongPhoneNumber) } shouldBe
                UserErrorException(PersonError.PHONE_NUMBER.toStatusBody())
    }

    "change phone number when valid" {
        shouldNotThrowAny { PersonService.changePhoneNumber(correctPerson1Entity.pesel, "669123111") }
    }

    "add person when his phone number have wrong format" {
        shouldThrowAny {
            PersonService.addPerson(wrongPersonDto)
        } shouldBe UserErrorsException(listOf(PersonError.PHONE_NUMBER.toStatusBody()))
    }

    "delete person when the person is not available in database" {
        shouldThrowAny {
            PersonService.deletePerson(peselNotAvailableInDatabase)
        } shouldBe UserErrorException(PersonError.NOT_FOUND.toStatusBody())
    }

    "delete person when the person is available in database" {
        shouldNotThrowAny {
            PersonService.deletePerson(correctPerson1Entity.pesel)
        }
    }
}) {
    override fun beforeSpec(spec: Spec) {
        startKoin {
            modules(
                listOf(
                    module {
                        single {
                            mockk<PersonRepository> {
                                every { getPersonByPesel(peselNotAvailableInDatabase) } throws UserErrorException(
                                    PersonError.PERSON_NOT_FOUND.toStatusBody()
                                )
                                every { getPersonByPesel(correctPerson1Entity.pesel) } returns correctPerson1Entity
                                every { changePhoneNumber(correctPerson1Entity.pesel, correctPhoneNumber) } returns Unit
                                every { deletePerson(peselNotAvailableInDatabase) } throws UserErrorException(PersonError.NOT_FOUND.toStatusBody())
                                every { deletePerson(correctPerson1Entity.pesel) } returns Unit
                            }
                        }
                    }
                )
            )
        }
    }

    override fun afterSpec(spec: Spec) {
        stopKoin()
    }

    companion object {
        const val peselNotAvailableInDatabase = "8112212333"
        const val wrongPhoneNumber = "23444444"
        const val correctPhoneNumber = "669123111"
        val wrongPersonDto = PersonDTO(firstName = "Jan", lastName = "Kowalski", phoneNumber = "77133256", pesel = "88112212331")
        val correctPerson1Entity = Person(firstName = "Jan", lastName = "Kowalski", phoneNumber = "771332531", pesel = "88112212331")
        val correctPerson1Dto = PersonDTO(firstName = "Jan", lastName = "Kowalski", phoneNumber = "771332531", pesel = "88112212331")
    }
}