package com.asseco.example.library.validator

import com.asseco.example.library.domain.dto.BookDTO
import com.asseco.example.library.exception.BookError
import com.asseco.example.library.exception.StatusBody
import com.asseco.example.library.exception.UserErrorsException

fun BookDTO.validate() = mutableListOf<StatusBody>().apply {
    validateIsbn(this@validate.isbn)?.let{ this.add(it) }

    if (this.isNotEmpty())
        throw UserErrorsException(this)
}

private fun validateIsbn(isbn: String) =
    if (isbn.toCharArray().fold(0, {acc, ch -> if (ch.isDigit()) acc + 1 else acc }) != 13)
        BookError.ISBN_LENGTH.toStatusBody()
    else null
