package com.asseco.example.library.validator

import com.asseco.example.library.domain.dto.PersonDTO
import com.asseco.example.library.exception.PersonError
import com.asseco.example.library.exception.StatusBody
import com.asseco.example.library.exception.UserErrorsException

fun PersonDTO.validate() = mutableListOf<StatusBody>().apply {
    validatePesel(this@validate)?.let { this.add(it) }
    phoneNumber.validatePhoneNumber()?.let { this.add(it.toStatusBody()) }

    if (this.isNotEmpty())
        throw UserErrorsException(this)
}

fun String.validatePhoneNumber() =
    if (this.length != 9) PersonError.PHONE_NUMBER else null

fun validatePesel(dto: PersonDTO): StatusBody? =
    if (dto.pesel.length != 11) PersonError.PESEL.toStatusBody() else null