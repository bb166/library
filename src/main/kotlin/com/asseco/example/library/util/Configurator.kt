package com.asseco.example.library.util

import org.koin.core.KoinComponent
import org.yaml.snakeyaml.Yaml
import java.io.InputStream
import java.lang.IllegalStateException
import java.util.*
import kotlin.collections.HashMap

object Configurator : KoinComponent {

    private val properties by lazy { HashMap<String, Any>().apply {
        putAll(parseDefaultPropertiesOrYaml())
        putAll(parseProfileProperties())
        putAll(parseProfileYaml())
        putAll(System.getenv())
    } }

    fun string(key: String): String = handleException(key) { properties[key] as String }
    fun int(key: String): Int = handleException(key) { properties[key] as Int }
    fun long(key: String): Long = handleException(key) { properties[key] as Long}
    fun bool(key: String): Boolean = handleException(key) { properties[key] as Boolean }
    @Suppress("UNCHECKED_CAST")
    fun array(key: String): Array<Any> = handleException(key) { properties[key] as Array<Any> }

    @Suppress("UNCHECKED_CAST")
    private fun parseDefaultPropertiesOrYaml(): Map<String, Any> {
        val propertiesFile: InputStream? = ClassLoader.getSystemResourceAsStream("config/application.properties")
        val yamlFile: InputStream? = ClassLoader.getSystemResourceAsStream("config/application.yml")

        return when {
            propertiesFile == null && yamlFile == null -> throw IllegalStateException("not found default configuration file")
            propertiesFile != null && yamlFile != null -> throw IllegalStateException("default configuration ambiguous")
            propertiesFile != null -> { propertiesFile.use { stream -> Properties().apply { load(stream) } } }
            yamlFile != null -> { yamlFile.use { (Yaml().load(it) as Map<String, Any>).flat() as Map<String, Any> } }
            else -> HashMap<String, Any>()
        } as Map<String, Any>
    }

    @Suppress("UNCHECKED_CAST")
    private fun parseProfileProperties() =
        Profile.values().filter { Profiles.has(it) }.map {
            ClassLoader.getSystemResourceAsStream("config/application-${it.toString().toLowerCase()}.properties")
                .use { stream -> stream?.let { Properties().apply { load(stream)  }} }
        }.fold(HashMap<Any, Any>(), { acc, el ->
            acc.putAll(el ?: Properties())
            acc
        }) as HashMap<String, Any>

    private fun parseProfileYaml() = Profile.values()
        .filter { Profiles.has(it) }
        .map {
            ClassLoader.getSystemResourceAsStream("config/application-${it.toString().toLowerCase()}.yml")
                .use { stream -> stream?.let { (Yaml().load(stream) as Map<String, Any>).flat() } ?: HashMap() }
        }.fold(hashMapOf<String, Any>(), { acc, el ->
            acc.putAll(el)
            acc
        })

    private fun Map<String, Any>.flat() = with (hashMapOf<String, Any>()) {
        flatRecursive(this@with, this@flat, "")
        this@with
    }

    @Suppress("UNCHECKED_CAST")
    private fun flatRecursive(result: MutableMap<String, Any>, map: Map<String, Any>, key: String): Unit = map.forEach{ (k, v) ->
        if (v !is Map<*, *>) result["$key$k"] = v
        else flatRecursive(result, v as Map<String, Any>, "$key$k.")
    }

    private fun <T> handleException(key: String, call: () -> T): T = try {
        call.invoke()
    } catch (e: NullPointerException) {
        throw IllegalAccessException("Property not found: $key.")
    } catch (e: ClassCastException) {
        throw IllegalAccessException("Wrong property type: $key.")
    }
}
