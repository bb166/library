package com.asseco.example.library.util

object Profiles {
    val profiles by lazy {
        (System.getenv("APPLICATION_PROFILE") ?: "").split(",").map { it.toUpperCase() }
    }
    fun has(profile: Profile) = profiles.contains(profile.name)
}

enum class Profile {
    POSTGRE
}
