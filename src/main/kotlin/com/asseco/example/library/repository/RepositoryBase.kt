package com.asseco.example.library.repository

import com.asseco.example.library.util.Configurator
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.lang.IllegalStateException

open class RepositoryBase : KoinComponent {
    private val configurator by inject<Configurator>()

    private val query: Map<String, Map<String, String>> = hashMapOf(
        "h2" to hashMapOf(
            SqlQueries.CHECK_BOOK_AVAILABILITY to H2SqlQueries.CHECK_BOOK_AVAILABILITY,
            SqlQueries.CHECK_BOOK_DELETE_STATE to H2SqlQueries.CHECK_BOOK_DELETE_STATE,
            SqlQueries.INSERT_BOOK to H2SqlQueries.INSERT_BOOK,
            SqlQueries.DELETE_BOOK to H2SqlQueries.DELETE_BOOK,
            SqlQueries.CHECK_PERSON_AVAILABILITY to H2SqlQueries.CHECK_PERSON_AVAILABILITY,
            SqlQueries.SELECT_RENTED_BOOKS_BY_PERSON to H2SqlQueries.SELECT_RENTED_BOOKS_BY_PERSON,
            SqlQueries.SELECT_ID_FOR_PERSON to H2SqlQueries.SELECT_ID_FOR_PERSON,
            SqlQueries.SELECT_ID_FOR_BOOK to H2SqlQueries.SELECT_ID_FOR_BOOK,
            SqlQueries.INSERT_BOOK_RENT_TO_PERSON to H2SqlQueries.INSERT_BOOK_RENT_TO_PERSON,
            SqlQueries.SELECT_PERSON_BY_PESEL to H2SqlQueries.SELECT_PERSON_BY_PESEL,
            SqlQueries.CHECK_PERSON_AVAILABILITY_IN_DATABASE to H2SqlQueries.CHECK_PERSON_AVAILABILITY_IN_DATABASE,
            SqlQueries.CHECK_PERSON_DELETE_STATE to H2SqlQueries.CHECK_PERSON_DELETE_STATE,
            SqlQueries.DELETE_PERSON to H2SqlQueries.DELETE_PERSON,
            SqlQueries.INSERT_PERSON to H2SqlQueries.INSERT_PERSON,
            SqlQueries.UPDATE_PERSON_PHONE_NUMBER to H2SqlQueries.UPDATE_PERSON_PHONE_NUMBER
        ),
        "postgres" to hashMapOf(
            SqlQueries.CHECK_BOOK_AVAILABILITY to PostgreSqlQueries.CHECK_BOOK_AVAILABILITY,
            SqlQueries.CHECK_BOOK_DELETE_STATE to PostgreSqlQueries.CHECK_BOOK_DELETE_STATE,
            SqlQueries.INSERT_BOOK to PostgreSqlQueries.INSERT_BOOK,
            SqlQueries.DELETE_BOOK to PostgreSqlQueries.DELETE_BOOK,
            SqlQueries.CHECK_PERSON_AVAILABILITY to PostgreSqlQueries.CHECK_PERSON_AVAILABILITY,
            SqlQueries.SELECT_RENTED_BOOKS_BY_PERSON to PostgreSqlQueries.SELECT_RENTED_BOOKS_BY_PERSON,
            SqlQueries.SELECT_ID_FOR_PERSON to PostgreSqlQueries.SELECT_ID_FOR_PERSON,
            SqlQueries.SELECT_ID_FOR_BOOK to PostgreSqlQueries.SELECT_ID_FOR_BOOK,
            SqlQueries.INSERT_BOOK_RENT_TO_PERSON to PostgreSqlQueries.INSERT_BOOK_RENT_TO_PERSON,
            SqlQueries.SELECT_PERSON_BY_PESEL to PostgreSqlQueries.SELECT_PERSON_BY_PESEL,
            SqlQueries.CHECK_PERSON_AVAILABILITY_IN_DATABASE to PostgreSqlQueries.CHECK_PERSON_AVAILABILITY_IN_DATABASE,
            SqlQueries.CHECK_PERSON_DELETE_STATE to PostgreSqlQueries.CHECK_PERSON_DELETE_STATE,
            SqlQueries.DELETE_PERSON to PostgreSqlQueries.DELETE_PERSON,
            SqlQueries.INSERT_PERSON to PostgreSqlQueries.INSERT_PERSON,
            SqlQueries.UPDATE_PERSON_PHONE_NUMBER to PostgreSqlQueries.UPDATE_PERSON_PHONE_NUMBER
        )
    )

    protected fun getQuery(queryName: String): String {
        val url = configurator.string("db.url")
        return when {
            url.contains("postgres", true) -> query["postgres"]!![queryName]!!
            url.contains("h2", true) -> query["h2"]!![queryName]!!
            else -> throw IllegalStateException("unknown database")
        }
    }
}