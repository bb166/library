package com.asseco.example.library.repository

object PostgreSqlQueries {
    const val CHECK_BOOK_AVAILABILITY = "select 1 from book where deleted = false and isbn = :isbn"
    const val INSERT_BOOK = "insert into book(isbn, title, pageCount, author, deleted) values (:isbn, :title, :pageCount, :author, false)"
    const val CHECK_BOOK_DELETE_STATE = "select 1 from book where deleted = true and isbn = :isbn"
    const val DELETE_BOOK = "update book set deleted = true where isbn = :isbn"
    const val CHECK_PERSON_AVAILABILITY = "select 1 from person where deleted = false and pesel = :pesel"
    const val SELECT_RENTED_BOOKS_BY_PERSON = "select b.* from book b join rented_book rb on rb.book_id = b.id join person u on u.id = rb.person_id where u.pesel = :pesel"
    const val SELECT_ID_FOR_PERSON = "select id from person where deleted = false and pesel = :pesel"
    const val SELECT_ID_FOR_BOOK = "select id from book where deleted = false and isbn = :isbn"
    const val INSERT_BOOK_RENT_TO_PERSON = "insert into rented_book(person_id, book_id) values (:personId, :bookId)"
    const val SELECT_PERSON_BY_PESEL = "select * from person where pesel = :pesel and deleted = false"
    const val CHECK_PERSON_AVAILABILITY_IN_DATABASE = "select 1 from person where pesel = :pesel"
    const val CHECK_PERSON_DELETE_STATE = "select 1 from person where deleted = true and pesel = :pesel"
    const val DELETE_PERSON = "update person set deleted = true where pesel = :pesel"
    const val INSERT_PERSON = "insert into person(firstName, lastName, phoneNumber, pesel, deleted) values (:firstName, :lastName, :phoneNumber, :pesel, false)"
    const val UPDATE_PERSON_PHONE_NUMBER = "update person set phoneNumber = :phoneNumber where pesel = :pesel"
}