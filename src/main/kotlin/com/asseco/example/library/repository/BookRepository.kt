package com.asseco.example.library.repository

import com.asseco.example.library.domain.entity.Book
import com.asseco.example.library.exception.BookError
import com.asseco.example.library.exception.PersonError
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.sql2o.Sql2o

object BookRepository: RepositoryBase() {
    private val sql2o by inject<Sql2o>()

    fun addBook(book: Book) = sql2o.open().use {
        val available = it.createQuery(getQuery(SqlQueries.CHECK_BOOK_AVAILABILITY))
            .addParameter("isbn", book.isbn)
            .executeScalar()

        if (available != null)
            BookError.ALREADY_AVAILABLE.throwException()

        it.createQuery(
            getQuery(SqlQueries.INSERT_BOOK)
        ).addParameter("isbn", book.isbn)
            .addParameter("title", book.title)
            .addParameter("pageCount", book.pageCount)
            .addParameter("author", book.author)
            .executeUpdate()

        Unit
    }

    fun deleteBook(isbn: String) = sql2o.open().use {
        it.createQuery(getQuery(SqlQueries.CHECK_BOOK_AVAILABILITY))
            .addParameter("isbn", isbn)
            .executeScalar() ?: BookError.NOT_FOUND.throwException()

        val alreadyDelete = it.createQuery(getQuery(SqlQueries.CHECK_BOOK_DELETE_STATE))
            .addParameter("isbn", isbn)
            .executeScalar()

        if (alreadyDelete != null)
            BookError.ALREADY_DELETED.throwException()

        it.createQuery(getQuery(SqlQueries.DELETE_BOOK))
            .addParameter("isbn", isbn)
            .executeUpdate()

        Unit
    }

    fun getRentedBooksForUser(pesel: String): List<Book> = sql2o.open().use {
        it.createQuery(getQuery(SqlQueries.CHECK_PERSON_AVAILABILITY))
            .addParameter("pesel", pesel)
            .executeScalar() ?: PersonError.PERSON_NOT_FOUND.throwException()

        it.createQuery(getQuery(SqlQueries.SELECT_RENTED_BOOKS_BY_PERSON))
            .addParameter("pesel", pesel)
            .executeAndFetch(Book::class.java)
    }

    fun rentBookByUser(pesel: String, isbn: String) = sql2o.open().use {
        val personId = (it.createQuery(getQuery(SqlQueries.SELECT_ID_FOR_PERSON))
            .addParameter("pesel", pesel)
            .executeScalar()  ?: PersonError.PERSON_NOT_FOUND.throwException()) as Int

        val bookId = (it.createQuery(getQuery(SqlQueries.SELECT_ID_FOR_BOOK))
            .addParameter("isbn", isbn)
            .executeScalar() ?: BookError.NOT_FOUND.throwException()) as Int

        it.createQuery(getQuery(SqlQueries.INSERT_BOOK_RENT_TO_PERSON))
            .addParameter("bookId", bookId)
            .addParameter("personId", personId)
            .executeUpdate()

        Unit
    }
}