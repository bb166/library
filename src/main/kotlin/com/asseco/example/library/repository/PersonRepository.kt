package com.asseco.example.library.repository

import com.asseco.example.library.domain.entity.Person
import com.asseco.example.library.exception.PersonError
import org.sql2o.Sql2o

object PersonRepository : RepositoryBase() {
    private val sql2o get() = getKoin().get<Sql2o>()

    fun getPersonByPesel(pesel: String): Person = sql2o.open().use {
        it.createQuery(getQuery(SqlQueries.SELECT_PERSON_BY_PESEL))
            .addParameter("pesel", pesel)
            .executeAndFetch(
                Person::class.java
            ).firstOrNull()
            ?: PersonError.PERSON_NOT_FOUND.throwException()
    }

    fun deletePerson(pesel: String) = sql2o.open().use {
        it.createQuery(getQuery(SqlQueries.CHECK_PERSON_AVAILABILITY_IN_DATABASE))
            .addParameter("pesel", pesel)
            .executeScalar() ?: PersonError.NOT_FOUND.throwException()

        val alreadyDelete = it.createQuery(getQuery(SqlQueries.CHECK_PERSON_DELETE_STATE))
            .addParameter("pesel", pesel)
            .executeScalar()

        if (alreadyDelete != null)
            PersonError.ALREADY_DELETED.throwException()

        it.createQuery(getQuery(SqlQueries.DELETE_PERSON))
            .addParameter("pesel", pesel)
            .executeUpdate()

        Unit
    }

    fun addPerson(entity: Person) = sql2o.open().use {
        val available = it.createQuery(getQuery(SqlQueries.CHECK_PERSON_AVAILABILITY_IN_DATABASE))
            .addParameter("pesel", entity.pesel)
            .executeScalar()

        if (available != null)
            PersonError.ALREADY_AVAILABLE.throwException()

        it.createQuery(getQuery(SqlQueries.INSERT_PERSON))
            .addParameter("firstName", entity.firstName)
            .addParameter("lastName", entity.lastName)
            .addParameter("phoneNumber", entity.phoneNumber)
            .addParameter("pesel", entity.pesel)
            .executeUpdate().close()
    }

    fun changePhoneNumber(pesel: String, phoneNumber: String) = sql2o.open().use {
        it.createQuery(getQuery(SqlQueries.CHECK_PERSON_AVAILABILITY))
            .addParameter("pesel", pesel)
            .executeScalar() ?: PersonError.PERSON_NOT_FOUND.throwException()

        it.createQuery(getQuery(SqlQueries.UPDATE_PERSON_PHONE_NUMBER))
            .addParameter("pesel", pesel)
            .addParameter("phoneNumber", phoneNumber)
            .executeUpdate()

        Unit
    }
}