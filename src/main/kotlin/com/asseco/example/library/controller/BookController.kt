package com.asseco.example.library.controller

import com.asseco.example.library.controller.metadata.BookMetadata
import com.asseco.example.library.domain.dto.BookDTO
import com.asseco.example.library.service.BookService
import de.nielsfalk.ktor.swagger.delete
import de.nielsfalk.ktor.swagger.get
import de.nielsfalk.ktor.swagger.post
import de.nielsfalk.ktor.swagger.version.shared.Group
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.response.respond
import io.ktor.routing.Routing
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.slf4j.LoggerFactory

@KtorExperimentalLocationsAPI
object BookController : KoinComponent {
    private val logger = LoggerFactory.getLogger(BookController.javaClass)

    private val bookService by inject<BookService>()

    private const val BASE_PATH = "/library/api"
    private const val GROUP = "book"

    @Group(GROUP)
    @Location("$BASE_PATH/books")
    class AddBook

    @Group(GROUP)
    @Location("$BASE_PATH/persons/{pesel}/books")
    data class GetRentedBooksForPerson(val pesel: String)

    @Group(GROUP)
    @Location("$BASE_PATH/books/{isbn}")
    data class DeleteBook(val isbn: String)

    @Group(GROUP)
    @Location("$BASE_PATH/persons/{pesel}/books/{isbn}")
    data class RentBookByUser(val isbn: String, val pesel: String)

    fun Routing.bookApi() {
        post<AddBook, BookDTO>(BookMetadata.addBook) { _, entity ->
            logger.info("Call add book behavior")
            bookService.addBook(entity)
            call.respond(HttpStatusCode.Created)
        }

        get<GetRentedBooksForPerson>(BookMetadata.getRentedBooksForPerson) { params ->
            logger.info("Call get rented books for person")
            call.respond(bookService.getRentedBooksForUser(params.pesel))
        }

        delete<DeleteBook>(BookMetadata.deleteBook) { params ->
            logger.info("Call delete book")
            bookService.deleteBook(params.isbn)
            call.respond(HttpStatusCode.OK)
        }

        post<RentBookByUser, Unit>(BookMetadata.rentBookByPerson) { params, _ ->
            logger.info("Call post rent book by person")
            bookService.rentBookByUser(params.pesel, params.isbn)
            call.respond(HttpStatusCode.OK)
        }
    }
}