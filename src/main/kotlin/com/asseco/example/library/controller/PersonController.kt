package com.asseco.example.library.controller

import com.asseco.example.library.controller.metadata.PersonMetadata
import com.asseco.example.library.domain.dto.PersonDTO
import com.asseco.example.library.service.PersonService
import de.nielsfalk.ktor.swagger.*
import io.ktor.routing.Routing
import de.nielsfalk.ktor.swagger.version.shared.Group
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.response.respond
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.slf4j.LoggerFactory

@KtorExperimentalLocationsAPI
object PersonController: KoinComponent {
    private val log = LoggerFactory.getLogger(PersonController.javaClass)

    private const val BASE_PATH = "/library/api/persons"
    private const val GROUP = "persons"

    private val userService by inject<PersonService>()

    @Group(GROUP)
    @Location(BASE_PATH)
    class AddUser

    @Group(GROUP)
    @Location("$BASE_PATH/{pesel}")
    data class GetUserByPesel(val pesel: String)

    @Group(GROUP)
    @Location("$BASE_PATH/{pesel}")
    data class DeleteUser(val pesel: String)

    @Group(GROUP)
    @Location("$BASE_PATH/{pesel}")
    data class ChangePhoneNumber(val pesel: String, val newPhoneNumber: String)

    fun Routing.userApi() {
        post<AddUser, PersonDTO>(PersonMetadata.addPerson) {
            _, entity ->
            log.info("Call add person")
            userService.addPerson(entity)
            call.respond(HttpStatusCode.Created)
        }

        get<GetUserByPesel>(PersonMetadata.getPersonByPesel) { params ->
            log.info("Call get person by pesel")
            call.respond(userService.getUserByPesel(params.pesel))
        }

        delete<DeleteUser>(PersonMetadata.deletePerson) { params ->
            log.info("Call delete person")
            userService.deletePerson(params.pesel)
            call.respond(HttpStatusCode.OK)
        }

        put<ChangePhoneNumber, Unit>(PersonMetadata.changePhoneNumber) { params, _ ->
            log.info("Call change phone number")
            userService.changePhoneNumber(params.pesel, params.newPhoneNumber)
            call.respond(HttpStatusCode.OK)
        }
    }
}