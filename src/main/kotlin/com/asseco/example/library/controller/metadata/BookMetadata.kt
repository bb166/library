package com.asseco.example.library.controller.metadata

import com.asseco.example.library.domain.dto.BookDTO
import de.nielsfalk.ktor.swagger.description
import de.nielsfalk.ktor.swagger.ok
import de.nielsfalk.ktor.swagger.responds

object BookMetadata {
    val addBook get() =
        "post"
            .description("add book to database")
            .responds(ok<Unit>())

    val getRentedBooksForPerson get() =
        "get"
            .description("get rented books for person")
            .responds(ok<Unit>())

    val rentBookByPerson get() =
        "post"
            .description("rent book by person")
            .responds(ok<Unit>())

    val deleteBook get() =
        "delete"
            .description("delete rented book by person")
            .responds(ok<Unit>())
}