package com.asseco.example.library.controller.metadata

import com.asseco.example.library.domain.dto.PersonDTO
import de.nielsfalk.ktor.swagger.created
import de.nielsfalk.ktor.swagger.description
import de.nielsfalk.ktor.swagger.ok
import de.nielsfalk.ktor.swagger.responds

object PersonMetadata {

    val getPersonByPesel get() =
        "get"
            .description("get person by pesel")
            .responds(ok<PersonDTO>())

    val deletePerson get() =
        "delete"
            .description("delete person by pesel")
            .responds(ok<Unit>())

    val addPerson get() =
        "post"
            .description("add new person to database")
            .responds(created<Unit>())

    val changePhoneNumber get() =
        "put"
            .description("change person phone number")
            .responds(ok<Unit>())
}