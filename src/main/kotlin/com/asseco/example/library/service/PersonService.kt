package com.asseco.example.library.service

import com.asseco.example.library.domain.dto.PersonDTO
import com.asseco.example.library.exception.UserErrorException
import com.asseco.example.library.exception.UserErrorsException
import com.asseco.example.library.mapper.toDto
import com.asseco.example.library.mapper.toEntity
import com.asseco.example.library.repository.PersonRepository
import com.asseco.example.library.validator.validate
import com.asseco.example.library.validator.validatePhoneNumber

import org.koin.core.KoinComponent
import org.koin.core.inject
import org.slf4j.LoggerFactory

object PersonService: KoinComponent {
    private val logger = LoggerFactory.getLogger(PersonService.javaClass)

    private val userRepository by inject<PersonRepository>()

    fun getUserByPesel(pesel: String) = handleException { userRepository.getPersonByPesel(pesel).toDto() }

    fun deletePerson(pesel: String) = handleException { userRepository.deletePerson(pesel) }

    fun addPerson(dto: PersonDTO) = handleException {
        dto.validate()
        userRepository.addPerson(dto.toEntity())
    }

    fun changePhoneNumber(pesel: String, phoneNumber:String) = handleException {
        phoneNumber.validatePhoneNumber()?.throwException()
        userRepository.changePhoneNumber(pesel, phoneNumber)
    }

    private fun <T> handleException(behavior: () -> T) = try {
        behavior.invoke()
    } catch (ex: UserErrorException) {
        logger.warn("User error: ${ex.statusBody}")
        throw ex
    } catch (ex: UserErrorsException) {
        logger.warn("User errors: ${ex.errors}")
        throw ex
    } catch (ex: Exception) {
        logger.error("Internal error", ex)
        throw ex
    }
}