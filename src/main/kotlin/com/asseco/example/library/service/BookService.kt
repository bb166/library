package com.asseco.example.library.service

import com.asseco.example.library.domain.dto.BookDTO
import com.asseco.example.library.exception.UserErrorException
import com.asseco.example.library.exception.UserErrorsException
import com.asseco.example.library.mapper.toDtos
import com.asseco.example.library.mapper.toEntity
import com.asseco.example.library.repository.BookRepository
import com.asseco.example.library.validator.validate
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.slf4j.LoggerFactory

object BookService : KoinComponent {
    private val logger = LoggerFactory.getLogger(BookService.javaClass)

    private val bookRepository by inject<BookRepository>()

    fun addBook(dto: BookDTO) = handleException {
        dto.validate()
        bookRepository.addBook(dto.toEntity())
    }

    fun deleteBook(isbn: String) = handleException { bookRepository.deleteBook(isbn) }

    fun getRentedBooksForUser(pesel: String): List<BookDTO> = handleException {
        bookRepository.getRentedBooksForUser(pesel).toDtos()
    }

    fun rentBookByUser(pesel: String, isbn: String) = handleException {
        bookRepository.rentBookByUser(pesel, isbn)
    }

    private fun <T> handleException(behavior: () -> T) = try {
        behavior.invoke()
    } catch (ex: UserErrorException) {
        logger.warn("User error: ${ex.statusBody}")
        throw ex
    } catch (ex: UserErrorsException) {
        logger.warn("User errors: ${ex.errors}")
        throw ex
    } catch (ex: Exception) {
        logger.error("Internal error", ex);
        throw ex
    }
}