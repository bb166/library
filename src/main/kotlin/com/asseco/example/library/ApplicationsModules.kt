package com.asseco.example.library

import com.asseco.example.library.repository.BookRepository
import com.asseco.example.library.repository.PersonRepository
import com.asseco.example.library.service.BookService
import com.asseco.example.library.service.PersonService
import com.asseco.example.library.util.Configurator
import com.mchange.v2.c3p0.ComboPooledDataSource
import org.koin.dsl.module
import org.sql2o.Sql2o

object ApplicationsModules {
    val modules = module {
        single(createdAtStart = true) {
            Configurator
        }
        single(createdAtStart = true) {
            val config by inject<Configurator>()
            val dataSource = ComboPooledDataSource()

            dataSource.user = config.string("db.user")
            dataSource.password = config.string("db.password")
            dataSource.jdbcUrl = config.string("db.url")
            dataSource.driverClass = config.string("db.driverClass")
            dataSource.initialPoolSize = config.int("c3p0.initialPoolSize")
            dataSource.minPoolSize = config.int("c3p0.minPoolSize")
            dataSource.maxPoolSize = config.int("c3p0.maxPoolSize")

            Sql2o(dataSource)
        }
    }

    val services = module {
        single(createdAtStart = true) {
            PersonService
        }
        single(createdAtStart = true) {
            BookService
        }
    }
    val repositories = module {
        single(createdAtStart = true) {
            PersonRepository
        }
        single(createdAtStart = true) {
            BookRepository
        }
    }
}