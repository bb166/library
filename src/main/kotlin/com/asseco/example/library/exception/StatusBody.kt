package com.asseco.example.library.exception

data class StatusBody(val code: Int, val msg: String)