package com.asseco.example.library.exception

enum class BookError(private val code: Int, private val msg: String) {
    //Data consistency
    ALREADY_DELETED(20, "Book is already deleted"),
    ALREADY_AVAILABLE(21, "Book is already available in database"),
    NOT_FOUND(22, "Book not found"),
    //Validation errors
    ISBN_LENGTH(110, "wrong isbn length")
    ;

    fun throwException(): Nothing = throw UserErrorException(StatusBody(code, msg))

    fun toStatusBody(): StatusBody = StatusBody(code, msg)
}