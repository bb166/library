package com.asseco.example.library.exception

import java.lang.RuntimeException

data class UserErrorException(val statusBody: StatusBody): RuntimeException(statusBody.msg)