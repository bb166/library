package com.asseco.example.library.exception

enum class PersonError(private val code: Int, private val msg: String) {
    //Data consistency
    ALREADY_DELETED(10,"person is already deleted"),
    ALREADY_AVAILABLE(11, "peron is already available in database"),
    NOT_FOUND(12, "person not found"),
    //Validation errors
    PESEL(105, "person validation error"),
    PERSON_NOT_FOUND(106, "user with given pesel number not found in database"),
    PHONE_NUMBER(107, "wrong phone number length")
    ;

    fun throwException(): Nothing = throw UserErrorException(StatusBody(code, msg))

    fun toStatusBody(): StatusBody = StatusBody(code, msg)
}