package com.asseco.example.library.exception

import java.lang.RuntimeException

data class UserErrorsException(val errors: List<StatusBody>) : RuntimeException()