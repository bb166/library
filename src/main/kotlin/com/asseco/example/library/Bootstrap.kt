package com.asseco.example.library

import com.asseco.example.library.controller.BookController.bookApi
import com.asseco.example.library.controller.PersonController.userApi
import com.asseco.example.library.exception.StatusBody
import com.asseco.example.library.exception.UserErrorException
import com.asseco.example.library.exception.UserErrorsException
import com.asseco.example.library.util.Profile
import com.asseco.example.library.util.Profiles
import de.nielsfalk.ktor.swagger.SwaggerSupport
import de.nielsfalk.ktor.swagger.version.v2.Swagger
import de.nielsfalk.ktor.swagger.version.v3.OpenApi
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.response.respond
import io.ktor.routing.routing
import org.koin.core.context.startKoin
import java.text.DateFormat

@KtorExperimentalLocationsAPI
fun Application.main() {
    startKoin {
        modules(
            listOf(
                ApplicationsModules.modules,
                ApplicationsModules.services,
                ApplicationsModules.repositories
            )
        )
    }
    install(SwaggerSupport) {
        path = "library/api/swagger-ui"
        forwardRoot = true
        swagger = Swagger()
        openApi = OpenApi()
    }
    install(Locations)
    install(DefaultHeaders)
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }
    install(StatusPages) {
        exception<Exception> {
            call.respond(HttpStatusCode.InternalServerError, StatusBody(-1, "Internal error: ${it.message}"))
        }
        exception<UserErrorException> {
            call.respond(HttpStatusCode.BadRequest, it.statusBody)
        }
        exception<UserErrorsException> {
            call.respond(HttpStatusCode.BadRequest, it.errors)
        }
    }
    routing {
        userApi()
        bookApi()
    }
}