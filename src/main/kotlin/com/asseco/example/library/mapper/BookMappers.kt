package com.asseco.example.library.mapper

import com.asseco.example.library.domain.dto.BookDTO
import com.asseco.example.library.domain.entity.Book

fun List<Book>.toDtos(): List<BookDTO> = this.map { it.toDto() }

fun Book.toDto() = BookDTO(
    isbn = this.isbn,
    title = this.title,
    pageCount = this.pageCount,
    author = this.author
)

fun BookDTO.toEntity() = Book(
    isbn = this.isbn,
    title = this.title,
    pageCount = this.pageCount,
    author = this.author
)