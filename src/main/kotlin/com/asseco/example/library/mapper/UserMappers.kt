package com.asseco.example.library.mapper

import com.asseco.example.library.domain.dto.PersonDTO
import com.asseco.example.library.domain.entity.Person

fun PersonDTO.toEntity() =
    Person(
        firstName = firstName,
        lastName = lastName,
        pesel = pesel,
        phoneNumber = phoneNumber
    )

fun Person.toDto() =
    PersonDTO(
        firstName = firstName,
        lastName = lastName,
        pesel = pesel,
        phoneNumber = phoneNumber
    )

