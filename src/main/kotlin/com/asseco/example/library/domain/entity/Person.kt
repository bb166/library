package com.asseco.example.library.domain.entity

data class Person(
    val id: Long? = null,
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val pesel: String,
    val deleted: Boolean? = null
)
