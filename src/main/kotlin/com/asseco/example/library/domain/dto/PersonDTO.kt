package com.asseco.example.library.domain.dto

data class PersonDTO(val firstName: String, val lastName: String, val phoneNumber: String, val pesel: String)