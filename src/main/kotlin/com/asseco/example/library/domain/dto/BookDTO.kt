package com.asseco.example.library.domain.dto

data class BookDTO(val isbn: String, val title: String, val pageCount: Int, val author: String)