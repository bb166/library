package com.asseco.example.library.domain.entity

data class Book(val id: Int? = null, val isbn: String, val title: String, val pageCount: Int, val author: String, val deleted: Boolean? = null)