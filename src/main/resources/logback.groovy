import ch.qos.logback.classic.encoder.PatternLayoutEncoder

appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "[%d{YYYY-MM-dd HH:mm:ss.SSS}] [%thread] %-5level %logger{36} - %msg%n"
    }
}

def level = (System.getenv("APPLICATION_PROFILES") ?: "").split("#").contains("DEV") ? DEBUG : INFO
root(level, ["STDOUT", "FILE"])
logger("org.eclipse.jetty", level)
logger("io.netty", level)
