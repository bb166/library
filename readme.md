# Library app
Celem powstania projektu, jest zaznajomienie zainteresowanych osób z językiem Kotlin oraz technologiami używanymi do implementacji usług dziedzinowych.

### Użyte framework'i i biblioteki
- **Ktor** - framework używany do tworzenia aplikacji, w oparciu o komunikacje z użyciem protokołu *HTTP*
- **Koin** - biblioteka realizująca wzorzec wstrzykiwana zależności
- **c3p0** - pula połączeń JDBC
- **sql2o** -  biblioteka osłaniająca sterownik JDBC w celu uproszczenia dostępu do danych zawartych w bazie danych
- **KotlinTest** - biblioteka umożliwiająca tworzenie testów jednostkowych
- **mockk** - biblioteka umożliwiająca tworzenia atrap obiektów w celu tworzenia izolowanych testów jednostkowych
- **LOGBack** - biblioteka realizujące funkcjonalność dzienników zdarzeń

Dodatkowo w projekcie zostały użyte dodatkowe biblioteki, które wymagane są w tej konkretnej implementacji:
- **h2 Database** - baza *in-memory*, wykorzystywana do uruchomienia aplikacji *out of the box*
- **PostgreSQL JDBC** - biblioteka implementująca interfejsy funkcjonalności JDBC, w przypadku uruchomienia aplikacji z użyciem narzędzia *Docker*

### Użyte narzędzia
- **Gradle** - narzędzie automatyzujące budowę aplikacji
- **Docker** - narzędzie wirtualizacji, służące do uruchamiania środowiska aplikacji
- **PostgreSQL** - *produkcyjna* baza danych aplikacji 

## Uruchamianie aplikacji
Domyślnie aplikacja uruchamiana jest na porcie 9991. W głównej ścieżce umieszczona została dokumentacja interfejsu API
### Z użyciem skryptu *gradlew*
Najprostszym sposobem uruchomienia aplikacji, jest uruchomienie jej z użyciem zadania *run*, narzędzia **Gradle**. W przypadku takiego sposobu uruchomienia, należy z użyciem terminalu systemowego przejść do katalogu, w którym umieszczona została zawartość repozytorium. Następnie wpisać polecenie 
```
./gradlew run
```
W przypadku wersji dla interpretera poleceń **bash** lub 
```
gradlew.bat run
```
w przypadku systemów z rodziny **Windows**

### Z użyciem narzędzia Docker
W przypadku tego sposobu, należy zbudować obraz, który zawierał będzie aplikacje. W tym celu, niezbędne jest zainstalowanie narzędzia Docker, wraz z rozszerzeniem Docker Compose. Następnie należy uruchomić terminal systemowy i przejść do katalogu, w którym umieszczona została zawartość repozytorium. Następnie wywołać polecenie
```
./gradlew buildDockerImage
```
w przypadku wersji dla interpretera poleceń **bash** lub 
```
gradlew.bat buildDockerImage
```
w przypadku systemów z rodziny **Windows**

Następnym krokiem jest uruchomienie aplikacji wraz z bazą danych. W tym celu należy z tego samego miejsca wywołać polecenie
```
docker-compose up
```
