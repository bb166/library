create table person(
                id serial,
                firstName varchar(255),
                lastName varchar(255),
                phoneNumber varchar(9),
                pesel varchar(11),
                deleted boolean
            );

insert into person(firstName, lastName, phoneNumber, pesel, deleted) values
            ('Jan', 'Kowalski', '669123123', '91111101231', false),
            ('Kamil', 'Nowak', '711312333', '88121191231', false);

create table book(
                id serial,
                isbn varchar(256),
                title varchar(512),
                pageCount int,
                author varchar(256),
                deleted boolean
            );

insert into book(isbn, title, pageCount, author, deleted) values
          ('978-83-01-14497-5', 'Człowiek istota społeczna', 543, 'Aronson Elliot', false),
          ('978-83-01-15304-5', 'Polityka społeczna: podręcznik akademicki', 777, 'G. Firlit-Fesnak.', false),
          ('83-228-0613-2','Człowiek w kulturze', 313, 'Krąpiec M. A.', false);

create table rented_book(person_id bigint, book_id bigint);